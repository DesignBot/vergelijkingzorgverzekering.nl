<?php
/*
 * Index template
 * DesignBot 20-10-2019
 */

get_header(); ?>

<section class="light">
    <div class="container inner-padding-y">
        <div class="row centered inner-padding-y">
            <?php for ($i=0; $i < 3; $i++) { ?>
                <div class="col usp-item s12 l3-5">
                    <div class="title">
                        <i class="far fa-address-card"></i>
                        <h3>Hoe vergelijken wij?</h3>
                    </div>
                    <p>
                        Wij hechten grote waarde aan goed en eerlijk 
                        vergelijken zodat u niet voor verrassingen komt te staan. Daarom lichten we graag toe hoe onze 
                        vergelijking werkt en waar wij u van dienst mee kunnen zijn.
                    </p>
                </div>
            <?php } ?>
        </div>

        <div class="row inner-padding-y-top">
            <?php for ($i=0; $i < 2; $i++) { ?>
                <div class="col s12 m12 l6">
                    <div class="cell top-list">
                        <div class="title">
                            <h2>Top zorgverzekeringen</h2>
                            <span>€385 eigen risico</span>
                        </div>

                        <ul>
                            <?php for ($o=0; $o < 5; $o++) { ?>
                                <li>
                                    <div class="image">
                                        <img src="<?= get_template_directory_uri() ?>/dist/img/logo.png">
                                    </div>
                                    <div class="content">
                                        <span>&euro; 107,25</span>
                                        <a class="waves-effect button primary">Bekijken</a>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<section>
    <div class="container inner-padding-y">
        <div class="row inner-padding-y">
            <div class="col s12 m12">
                <h3>Nam pretium turpis et arcu</h3>
                <p>Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi.</p>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m12">
                <div class="cell tip inner-padding center-align">
                    <h3>Tip: twijfel je of je wilt overstappen? Zeg je huidige verzekering voor 31 december op. <br>Je hebt dan nog een hele maand voor je definitieve keuze!</h3>
                </div>
            </div>
        </div>

        <div class="row centered inner-padding-y">
            <div class="col s12 m5-5">
                <h3>Zorgverzekeringen vergelijken</h3>
                <p>Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo.  Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis.</p>
            </div>
            <div class="col s12 m5-5">
                <h3>Zorgverzekeringen vergelijken</h3>
                <p>Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo.  Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis.</p>
            </div>
        </div>

        <div class="row inner-padding-y">
            <div class="col s12 m12">
                <div class="cell best-list">
                    <div class="title">
                        <h2>De beste keus voor jou</h2>
                    </div>

                    <ul>
                        <?php for ($i=0; $i < 6; $i++) { ?>
                        <li>
                            <img src="<?= get_template_directory_uri() ?>/dist/img/logo.png">
                            <span>&euro; 107,25</span>
                            <div class="rating">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                            </div>
                            <a class="waves-effect button primary large">Bekijken</a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row inner-padding-y centered">
            <?php for ($i=0; $i < 3; $i++) { ?>
                <div class="col offer-item s12 m4 l3-5">
                    <div class="content">
                        <div class="image">
                            <img src="https://www.lc.nl/images/94ini1-B82387768Z.1_20110621075634_000GG67NO5I.2.jpg/ALTERNATES/WIDE_768/B82387768Z.1_20110621075634_000+GG67NO5I.2.jpg">
                        </div>
                        <div class="price">
                            <h1>&euro; 107,25</h1>
                            <span>Eigen risico &euro; 385,-</span>
                        </div>
                    </div>
                    <a class="waves-effect button secondary medium">Bekijk deze verzekering</a>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<section class="light">
    <div class="container inner-padding-y">
        <div class="row inner-padding-y centered">
            <?php for ($i=0; $i < 3; $i++) { ?>
                <div class="col s12 m3-5">
                    <h4>Zorgverzekeringen vergelijken</h4>
                    <p>
                        Curabitur ligula sapien, tincidunt non
                        Maecenas malesuada 
                        Praesent congue erat at massa
                        Sed cursus turpis vitae tortor
                        Donec posuere vulputate arcu
                        Phasellus accumsan cursus velit
                    </p>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<?php 
// WP Bakery content
the_content();

// Footer
get_footer();