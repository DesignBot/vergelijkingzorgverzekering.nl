<?php 
/*
 * Archive template
 * DesignBot 19-1-2019
 */

get_header(); ?>

<main id="primary" class="page-primary" role="main">
    <div class="col-full">
        <?php if (have_posts()) : ?>
            <h1 class="page-title"><?php the_archive_title(); ?></h1>

            <?php get_template_part( 'loop' ); ?>

        <?php else : get_template_part('content', 'none'); ?>
    </div>    
</main>

<?php get_footer(); ?>