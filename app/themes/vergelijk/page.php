<?php
/*
 * Page template
 * DesignBot 20-10-2019
 */

get_header();

// WP Bakery content
the_content();

// Footer
get_footer();