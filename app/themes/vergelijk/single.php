<?php 
/*
 * Single page template
 * DesignBot 3-1-2019
 */

get_header(); ?>

<main id="primary" class="page-primary" role="main">
    <div class="col-full">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part('content', 'single'); ?>
        <?php endwhile; ?>
    </div>
</main>

<?php get_footer(); ?>