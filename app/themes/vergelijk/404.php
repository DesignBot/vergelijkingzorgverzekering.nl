<?php 
/*
 * 404 template
 * DesignBot 3-1-2019
 */

get_header(); ?>

<main id="primary" class="page-primary" role="main">
    <div class="col-full">
    	<h1 class="page-title">Pagina niet gevonden</h1>

        <div class="no-content content-section">
            <p>Het lijkt er op dat wat u zocht niet op deze plek staat.<br>Doorzoek de site of ga terug naar de <a href="<?php echo home_url(); ?>">homepagina</a>.</p>
        </div>
    </div>
</main>

<?php get_footer(); ?>