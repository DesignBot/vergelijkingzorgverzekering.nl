<?php 
/*
 * Footer template
 * DesignBot 20-10-2019
 */

wp_footer() ?>

		</main>

		<footer class="center-align">
			<span>&copy; Copyright <?= date('Y') ?> NoordhuisMiljonair BV</span>
		</footer>
	</body>
</html>