<?php 
/*
 * Search results
 * DesignBot 5-11-2018
 */

get_header(); ?>

<main id="primary" class="page-primary" role="main">

    <div class="col-full">
    	<?php if ( have_posts() ) : ?>
    		<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'woocommerce' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
    		<?php get_template_part( 'loop' ); ?>
    	<?php else : ?>
    		<?php get_template_part( 'content', 'none' ); ?>
    	<?php endif; ?>
    </div>
	
</main>

<?php get_sidebar(); ?>
<?php get_footer(); ?>