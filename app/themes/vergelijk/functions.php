<?php
/**
 * Functions
 * DesignBot 20-10-2019
 */

// include main.js
if (!is_admin()) {
    function load_main_script() {
        wp_register_script( 'main', get_template_directory_uri() . '/dist/js/main.js', array('jquery'), '2.0', true );
        wp_enqueue_script( 'main' );
    }
    add_action('wp_enqueue_scripts', 'load_main_script'); 
}

// Add theme support
add_theme_support( 'title-tag' );
add_theme_support( 'woocommerce' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );

// Add HTML5 support
add_theme_support( 'html5', array(
    'comment-form',
    'comment-list',
    'search-form',
    'caption',
    'gallery',
    'widgets',
));

// Register menus
register_nav_menus( array(
    'main'      => 'Main menu',
    'footer'    => 'Main menu',
));
        
// Custom menu with materialize sidenav support
function get_the_menu($type) {
    global $post;
    $menu_list  = 0;
    $current    = $post->ID;

    if (($locations = get_nav_menu_locations()) && isset($locations['main'])) :
        $menu       = wp_get_nav_menu_object($locations['main']);
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $menu_list  = $type == 'main' ? '<ul class="hide-on-med-and-down">' : ($type == 'mobile' ? '<ul class="sidenav" id="mobile-menu">' : '<ul>');

        foreach ((array) $menu_items as $key => $menu_item):
            $title      = $menu_item->title;
            $url        = $menu_item->url;
            $id         = $menu_item->object_id;
            $state      = $id == $current ? 'class="active"' : false;
            $menu_list .= '<li '. $state .'><a href="'. $url .'" class="waves-effect">'. $title .'</a></li>';
        endforeach;

        $menu_list .= '</ul>';
    endif;

    echo $menu_list;
}