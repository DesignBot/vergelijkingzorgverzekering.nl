<?php 
/*
 * Sidebar template
 * DesignBot 14-2-2019
 */
?>

<aside class="filters" id="sidebar-filters">
    <div class="filters-wrapper">
        <div class="filters-spacer hidden-sm" id="close-filters"></div>

        <form class="filters-container" id="form-filters" method="get">
            <?php 
                $search             = isset($_POST['s']) ? $_POST['s'] : false;
                $term               = !is_shop() ? get_queried_object() : false;
                $current_tax_page   = is_object($term) ? $term->slug : false;
                $product_cat        = is_object($term) ? $term->term_id : false;

                $term_id            = is_object($term) ? $term->term_id : false;
                $term_tax           = is_object($term) ? $term->taxonomy : false;

                // echo $term_id, $term_tax;

                // Get parent category slug
                $parent_slug = get_term($term->parent, 'product_cat'); 


            if (is_post_type_archive('product') || is_tax( array('product_cat', 'product_tag') ) || is_product_taxonomy() || is_search()) :
            
                $categories             = array();
                $url_parameters         = php_fix_raw_query();
                $active_filter_count    = 0;
                $front_id               = get_option('page_on_front');
                $selected_filters       = get_field('filters', $front_id);
                $query_params           = array( 'relation' => 'OR' );
                $search_term            = '';

                $meta_cat               = get_term_meta( $product_cat, 'category_filters', true );
                $meta_term              = get_term_meta( $product_cat, 'filters', true );
                $get_filters            = $meta_cat != '' ? $meta_cat : ( $meta_term ? $meta_term : false );

                // Get the terms for products in category
                if ($meta_cat && $product_cat) {
                    $query_params[] = array(
                        'taxonomy'      => 'product_cat',
                        'field'         => 'term_id',
                        'terms'         => $product_cat,
                        'operator'      => 'IN'
                    );
                }

                if ($meta_term && $term_tax) {
                    $query_params[] = array(
                        'taxonomy'      => $term_tax,
                        'field'         => 'term_id',
                        'terms'         => $term_id,
                        'operator'      => 'IN'
                    );
                }

                if ($search) $search_term = array('s' => $search);

                $args = array(
                    'posts_per_page'    => 600,
                    'tax_query'         => array( $query_params ),
                    'post_status'       => 'publish',
                    'post_type'         => 'product',
                    'fields'            => 'ids',
                    'orderby'           => 'date',
                    'order'             => 'DESC',
                    $search_term
                );

                $query = new WP_Query( $args );

                if ($get_filters) :

                    foreach ($get_filters as $key => $value) {
                        $categories[] = 'pa_'. $value;
                    }

                else :

                    // Loop through posts to get active taxonomies
                    if ( $query->have_posts() ) {
                        foreach ($selected_filters as $key => $term) {
                            $set = false;
                            
                            foreach ($query->posts as $post_id) {
                                if ( is_object_in_term( $post_id, $term ) && !$set) {
                                    $categories[] = $term;
                                    $set = true;
                                }

                                if ($set) break;
                            }
                        }

                        wp_reset_postdata();
                    }

                endif;

                // Set active filtercount on load
                foreach ($url_parameters as $key => $value) {
                    if (\strpos($key, 'pa_') !== false) {
                        $active_filter_count ++;
                    }
                }

                $car_selection      = array();
                $car_selection[1]   = 'pa_merk';
                $car_selection[2]   = 'pa_model';
                $car_selection[3]   = 'pa_model-type';

                if (in_array('pa_merk', array_column($categories, 'value')) || in_array('pa_merk', $categories)) : ?> 

                    <div class="filter" id="car-filter">
                        <header>
                            <h3>Zoek op auto</h3>

                            <div class="toggle">
                                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="6" viewBox="0 0 10 6">
                                    <polygon points="1 0 5 4 9 0 10 1 5 6 0 1"/>
                                </svg>
                            </div>
                        </header>

                        <div class="form-controls">

                        <?php foreach ($car_selection as $key => $value) {
                            $terms = get_terms( array(
                                'taxonomy' => $value,
                                'hide_empty' => false,
                            ));

                            $select_key = isset($_POST[$car_selection[1]]) || $value == 'pa_merk' ? true : false;
                            $label = 'Kies '. ($value == 'pa_merk' ? 'automerk' : ($value == 'pa_model' ? 'model' : ($value == 'pa_model-type' ? 'type' : ''))); ?>

                            <div class="select-wrapper car-select-wrapper <?= !$select_key ? 'hidden' : ''; ?>">
                                <select class="car-select" name="<?= $value ?>" id="<?= $value ?>">
                                    <option value="null"><?= $label ?></option>
                                    <?php foreach ($terms as $key => $item) :
                                        $checked = false;
                                        $f_tax = array();

                                            if (array_key_exists($item->taxonomy, $url_parameters)) {
                                                foreach ($url_parameters as $key => $value) {
                                                    $f_tax[] = $value;
                                                }
                                            }

                                            $checked = ( in_array($item->slug, $f_tax) || $current_tax_page == $item->slug ) ? 'selected' : '';
                                        ?>

                                        <option value="<?= $item->slug ?>" <?= $checked ? $checked : ''; ?>><?= wc_attribute_label( $item->name ) ?></option>
                                    <?php endforeach; ?>
                                </select>

                                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="6" viewBox="0 0 10 6">
                                    <polygon points="1 0 5 4 9 0 10 1 5 6 0 1"></polygon>
                                </svg>
                            </div>

                            <?php } ?>
                        </div>
                    </div>

                <?php endif;

                $get_taxonomies = attribute_taxonomies( $categories );

                // var_dump($query->posts);

                if ($get_taxonomies) :
                    // var_dump($get_taxonomies);
                    foreach ($get_taxonomies as $label => $taxonomy) :

                        // echo $taxonomy;

                        $terms      = $meta_cat != '' ? wp_get_object_terms($query->posts, $taxonomy) : wp_get_object_terms($query->posts, $taxonomy);
                        $has_input  = in_array($taxonomy, $url_parameters) ? 'has-input' : '';
                        $shown      = $search || is_shop() ? 'active' : '';

                        // var_dump($query->posts, get_the_terms($query->posts, $taxonomy));

                        if ($terms) : ?>
                            <div class="filter <?= $has_input .' '. $shown ?>">
                                <header>
                                    <h3><?= $label ?></h3>

                                    <div class="remove">wissen</div>

                                    <div class="toggle">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="10" height="6" viewBox="0 0 10 6">
                                            <polygon points="1 0 5 4 9 0 10 1 5 6 0 1"/>
                                        </svg>
                                    </div>
                                </header>

                                <ul class="form-controls">
                                    <?php foreach ($terms as $key => $item) { ?>
                                        <li>
                                            <label for="<?= $item->slug ?>">
                                                <?php // check if checked
                                                    $f_tax = array();
                                                    $checked = false;

                                                    if (array_key_exists($item->taxonomy, $url_parameters)) :
                                                        foreach ($url_parameters as $key => $value) {
                                                            $f_tax[] = $value;
                                                        }

                                                        $checked = (in_array($item->slug, $f_tax)) ? 'checked' : '';
                                                    endif;
                                                ?>

                                                <input type="checkbox" name="<?= $item->taxonomy ?>" id="<?= $item->slug ?>" value="<?= $item->slug ?>" <?= $checked ? $checked : ''; ?>><!--  onchange="this.form.submit()" --> <?php // echo in_array($item->slug, $f_tax) ? 'checked' : ''; ?>

                                                <div class="icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="10" viewBox="0 0 11 10">
                                                        <polygon points="3.9 7.4 9.5 0 11 1.1 4.2 10 0 6.7 1.1 5.2"/>
                                                    </svg>
                                                </div>

                                                <span><?= wc_attribute_label( $item->name ) ?></span>
                                            </label>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </div>
                        <?php endif;
                    endforeach;
                endif;
            endif; ?>

            <?php if ($parent_slug) { ?><input type="hidden" id="parent-category-filter" name="parent-category" value="<?= $parent_slug->slug ?>"><?php } ?>
            <?php if ($product_cat && $meta_cat) { ?>
                <input type="hidden" id="category-filter" name="category" value="<?= $product_cat ?>">
            <?php } else { ?>
                <input type="hidden" id="term_tax-filter" name="term_tax" value="<?= $term_tax ?>">
                <input type="hidden" id="term_id-filter" name="term_id" value="<?= $term_id ?>">
            <?php } ?>
            <?php if ($search) { ?><input type="hidden" id="search-filter" name="s" value="<?= $search ?>"><?php } ?>
        </form>
    </div>
    
    <div class="actions filters-actions">
        <?php button('Annuleren', '', 'button', 'secondary', '', 'cancel-filters', '') ?>
        <?php button('Toepassen', '', 'button', 'primary', '', 'submit-filters', '') ?>
    </div>

    <div class="filter-button-fixture">
        <?php button('Filter producten<span class="count" id="count-filters" data-count="'. $active_filter_count .'">'. $active_filter_count .'</span>', '#', 'button filters', '', '', 'toggle-filters', '') ?>
    </div>
</aside>
