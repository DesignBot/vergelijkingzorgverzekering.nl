<?php 
/*
 * Main menu
 * DesignBot 20-10-2019
 */

global $post;
$current = $post->ID;
$menu_list = 0;

if (($locations = get_nav_menu_locations()) && isset($locations['main'])) :
    $menu       = wp_get_nav_menu_object($locations['main']);
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    $menu_list  = '<ul class="sidenav" id="mobile-menu">';

    foreach ((array) $menu_items as $key => $menu_item):
        $title      = $menu_item->title;
        $url        = $menu_item->url;
        $id         = $menu_item->object_id;
        $state      = $id == $current ? 'class="active"' : false;
        $menu_list .= '<li '. $state .'><a href="'. $url .'" class="waves-effect">'. $title .'</a></li>';
    endforeach;

    $menu_list .= '</ul>';
endif;

echo $menu_list;