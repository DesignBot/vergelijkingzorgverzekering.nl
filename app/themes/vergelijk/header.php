<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>		
	<?php wp_head(); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/dist/css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body <?php body_class(); ?>>

    <div class="header-fixture">
        <header class="navbar">
            <nav>
                <div class="container">
                    <div class="nav-wrapper">
                        <a href="<?= home_url() ?>" class="logo">
                        	<div class="logomark">
                            	<img src="<?= get_template_directory_uri() ?>/dist/img/logo.png">
                            </div>

                            <div class="wordmark">
                            	<span>Vergelijking</span>
                            	<span>zorgverzekering.nl</span>
                            </div>
                        </a>

                        <a href="#" data-target="mobile-menu" class="sidenav-trigger waves-effect">
                            <i class="fas fa-bars"></i>
                        </a>

                        <?php get_the_menu('main') ?>
                    </div>
                </div>
            </nav>
        </header>
    </div>

    <?php get_the_menu('mobile') ?>

    <main>